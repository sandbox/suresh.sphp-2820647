CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Permissions
 * Usage
 * Theming

INTRODUCTION
------------

Current Maintainers:

 * ZenDoodles http://drupal.org/user/226976
 * cweagans http://drupal.org/user/404732
 * Devin Carlson http://drupal.org/user/290182


REQUIREMENTS
------------

Crude Oil Block has one dependency.

Drupal core modules
 * Block


