
/**
 * Crude Oil Block
 */

(function ($) {

  "use strict";

   Drupal.behaviors.crudeoil_block = {
    attach: function (context,settings) {
	  $('.crude-oil div div.thumbs').next().remove();
    },
    detach: function (context) {}
  };

})(jQuery);
